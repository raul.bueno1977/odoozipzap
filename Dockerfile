# Usar Ubuntu 18.04.6 LTS como imagen base
FROM ubuntu:18.04

LABEL maintainer="RGB"

# Evitar interacciones en la línea de comandos
ENV DEBIAN_FRONTEND=noninteractive

# Actualizar e instalar dependencias necesarias
RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    python3.6 \
    python3-pip \
    python3-dev \
    libxml2-dev \
    libxslt1-dev \
    libldap2-dev \
    libsasl2-dev \
    libtiff5-dev \
    libjpeg8-dev \
    zlib1g-dev \
    libfreetype6-dev \
    liblcms2-dev \
    libwebp-dev \
    libharfbuzz-dev \
    libfribidi-dev \
    libxcb1-dev \
    libpq-dev \
    git \
    node-less \
    gcc \
    libssl-dev \
    libffi-dev \
    zbar-tools \
    python3-requests \
    libmagic1 \
    python3-magic \
    wget \
    xfonts-75dpi \
    xfonts-base \
    libxrender1 \
    libfontconfig1 \
    fontconfig \
    libjpeg-turbo8 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*


# Actualizar pip y luego instalar setuptools
RUN python3.6 -m pip install --upgrade pip && \
    python3.6 -m pip install 'setuptools<58'

# Descargar e instalar la versión de wkhtmltopdf compatible con Ubuntu 18.04 y Odoo 11
RUN  wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.bionic_amd64.deb \
    && dpkg -i wkhtmltox_0.12.5-1.bionic_amd64.deb \
    && cp /usr/local/bin/wkhtmltopdf /usr/bin/ \
    && cp /usr/local/bin/wkhtmltoimage /usr/bin/

# Clonar Odoo 11 desde GitHub
RUN git clone --depth=1 --branch=11.0 https://www.github.com/odoo/odoo.git /opt/odoo

# Instalar dependencias de Python para Odoo 11
RUN python3.6 -m pip  install -r /opt/odoo/requirements.txt

# Instalar paquetes adicionales de Python si son necesarios
COPY ./zipzap_requirements.txt /opt/odoo/zipzap_requirements.txt

RUN python3.6 -m pip  install -r /opt/odoo/zipzap_requirements.txt

# Agregar un usuario no root y cambiar a él
RUN adduser --disabled-password --gecos "" odoo

COPY ./odoo.conf /etc/odoo/

# Crear directorios y ajustar permisos
RUN mkdir -p /var/lib/odoo /mnt/extra-addons /var/lib/odoo/filestore \
    && chown -R odoo:odoo /var/lib/odoo /mnt/extra-addons /etc/odoo/odoo.conf

# Configurar el directorio de trabajo y el punto de entrada
USER odoo
WORKDIR /opt/odoo

CMD ["python3", "/opt/odoo/odoo-bin", "-c", "/etc/odoo/odoo.conf"]
